Sources I used to compile this code:
https://www.w3schools.com/html/html_intro.asp to copy down the basic html structure
https://www.w3schools.com/tags/tag_meta.asp copied meta tags for index.html
added script tag from last week's assignment
https://www.w3schools.com/tags/tag_link.asp copied and pasted css link
https://www.google.com/search?q=how+to+make+a+list+on+html+horizontal&oq=how+to+make+a+list+on+html+horizontal&aqs=chrome..69i57j0i22i30l2.10820j0j7&sourceid=chrome&ie=UTF-8 to refresh on inline 
https://www.w3schools.com/css/css_padding.asp refresh on padding shorthand
https://www.w3schools.com/css/css_border_color.asp help fix border complications
https://stackoverflow.com/questions/13848482/border-does-not-show-up help fix border complications
https://www.google.com/search?q=why+are+my+borders+not+showing+on+css&oq=why+are+my+borders+not+showing+on+css&aqs=chrome..69i57j0i22i30j0i390.14492j0j7&sourceid=chrome&ie=UTF-8 help fix border complications
https://www.w3schools.com/css/css3_flexbox_responsive.asp flexbox refresher
https://www.w3schools.com/css/css_rwd_mediaqueries.asp media query refresher



I had trouble with toggling between the classes to make a color change, so I used several outside resources to inform my coding decisions: 

https://www.w3schools.com/js/tryit.asp?filename=tryjs_loop_for
https://www.w3schools.com/js/js_loop_for.asp
https://www.w3schools.com/js/js_loop_for.asp
https://www.w3schools.com/js/tryit.asp?filename=tryjs_loop_for_ex
https://www.w3schools.com/js/tryit.asp?filename=tryjs_loop_for_om1
https://www.linkedin.com/learning/javascript-enhancing-the-dom/controlling-classes-with-the-html5-classlist?
https://www.w3schools.com/html/html_colors.asp
https://stackoverflow.com/questions/1819878/changing-button-color-programmatically
https://www.w3schools.com/js/js_function_call.asp
https://www.w3schools.com/jsref/dom_obj_event.asp


I copied the append node code from an example in W3Schools:
https://www.w3schools.com/jsref/tryit.asp?filename=tryjsref_node_appendchild


I'm having a lot of trouble getting a button to apppear when a box is clicked and then resetting the page:
https://www.w3schools.com/js/tryit.asp?filename=tryjs_dom_elementremove i copied this from W3Schools to try to remove the alerts
https://www.w3schools.com/js/js_htmldom_nodes.asp
https://www.w3schools.com/tags/tryit.asp?filename=tryhtml5_a_href_script
https://www.w3schools.com/tags/tag_a.asp#:~:text=The%20tag%20defines%20a,which%20indicates%20the%20link's%20destination. figuring out what to tag in my button on html to make the content toggle properly 
https://www.w3schools.com/css/tryit.asp?filename=trycss_buttons_basic I copied and pasted the button html code and put it into my index.html
https://www.w3schools.com/css/css3_buttons.asp
https://www.w3schools.com/jsref/met_node_appendchild.asp
https://www.w3schools.com/jsref/tryit.asp?filename=tryjsref_node_appendchild copied myFunction text to append child in my JS file
https://www.w3schools.com/jsref/tryit.asp?filename=tryjsref_node_appendchild2 tried onclick code to get my button to reset the page 
https://www.w3schools.com/js/js_htmldom_nodes.asp used remove() to try to remove the alerts and reset the page
https://www.w3schools.com/js/tryit.asp?filename=tryjs_addeventlistener_add_many used this code to assist me in constructing a click function to the 1,2,3 boxes and make alerts appear
https://www.w3schools.com/js/js_htmldom_eventlistener.asp
